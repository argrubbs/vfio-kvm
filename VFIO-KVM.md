# VFIO KVM Notes
## Hardware Considerations

- Use Nvidia GPU
- At least 32GB RAM
- At least 6c/12t CPU
- Extra USB controller to pass to VM
- Second monitor for VM
- Second keyboard and mouse set for VM
- Wired Ethernet adapter

## BIOS/UEFI settings

Enable `Virtualization`, `VT-d` and `Over 4G Encoding` in UEFI

## Bridge Networking config

`/etc/systemd/network/br.netdev`

```
[NetDev]
Name=br0
Kind=bridge
```

`/etc/systemd/network/1-br0-bind.network`
```
[Match]
Name=enp0s31f6

[Network]
Bridge=br0
```

**Replace interface Name with actual ethernet interface**

`/etc/systemd/network/2-br0.dhcp.network`
```
[Match]
Name=br0

[Network]
DHCP=ipv4
```

Disable NetworkManager and enable networkd

```
sudo systemctl disable NetworkManager
sudo systemctl enable --now networkd
```

## GRUB boot flags

`GRUB_CMDLINE_LINUX_DEFAULT="fsck.mode=skip i915.enable_guc=2 quiet splash intel_iommu=on kvm_ignore_msrs=1 vfio-pci.ids=1461:0054,10de:0fbb,10de:13c2,8086:a2af pcie_acs_override=downstream"`

Add `intel_iommu=on` and `kvm_ignore_msrs=1` and `pcie_acs_override=downstream`

`vfio-pci.ids=` should be a comma separated list of PCI device IDs to pass into the VM

Find these IDs with `lspci -nnv`

Run `sudo update-grub` and reboot

## Kernel

ACS patch is required for IOMMU isolation

Liquorix kernel provides this and is the easiest to install on Ubuntu-based machines

Use the following to discover IOMMU groups

```bash
#!/bin/bash
# change the 999 if needed
shopt -s nullglob
for d in /sys/kernel/iommu_groups/{0..999}/devices/*; do
    n=${d#*/iommu_groups/*}; n=${n%%/*}
    printf 'IOMMU Group %s ' "$n"
    lspci -nns "${d##*/}"
done;
```
---
## Host Machine Packages

Install the following

```
sudo apt install qemu-kvm qemu-utils libvirt-daemon-system libvirt-clients bridge-utils virt-manager ovmf
```

---
## Guest Windows VM Setup

Launch `virt-manager`

Create a new local VM

Select ISO file. May need to `Browse Local` to find the image elsewhere.

Set RAM and CPU

### Storage

Create a virtual hard drive image with the following in the directory you want the hard drive

`fallocate -l 10G win10.img`

---

Assign the storage above to the VM by selecting `Select of create custom storage`

**CHECK Customize configuration before install**

Select host-bridge network that was created earlier for network

Add the following to hide the VM from the GPU driver:

```xml
<features>
    ...
    <vendor_id state='on' value='abc123abc345'/>
    ...
</features>
...
<kvm>
    <hidden state='on'/>
</kvm>
```

---

### Guest VM Config

#### Overview

Set Firmware to `OVMF_CODE`
Use `ms` for Win 10/11
Otherwise use vanilla `OVMF_CODE` firmware

#### CPU

Set topology as follows:

Ensure that `host-passthrough` is set

```
Socket -> 1
Cores -> 6
Threads -> 2
```

Socket should always be set to 1.
`Threads` should correspond to the number of threads **PER CORE**

6 cores, 2 threads = 12 vCPUs

#### Storage

Set Disk Drive to `virtio`

Set virtio hard drive cache mode to `unsafe` for fastest performance

Create new CD-ROM device and select the virtio driver ISO

[VirtIO Drivers ISO Link](https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/latest-virtio/virtio-win.iso)

#### PCI Devices

Click `Add Hardware` and select `PCI Host Device`

Add in the devices that you isolated above

#### Windows Install

When selecting a hard drive to install to choose `Load Driver` from the bottom left

Browse to the `virtio` driver CD and select the `amd64` folder and the `w10` or `w11` folder as needed

Hard drive should show up and be available

Install Nvidia drivers and other devices as normal

**NOTE**
**DO NOT INSTALL WSL2**
**THIS BREAKS NVIDIA DRIVER**
**NOTE**