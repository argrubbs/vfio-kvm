# VFIO KVM Notes

This is a repo with some notes on how to get KVM VFIO working

**This is meant for my personal machine, so tailor it to your own hardware as needed**

[virtio latest driver iso](https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/latest-virtio/virtio-win.iso)

[Liquorix Kernel Download](https://liquorix.net/#install)

Enjoy!